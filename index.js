const fetch = require('node-fetch');

class Cryptovouvher {
    constructor(api_key, api_secret) {
        this.api_key = api_key;
        this.api_secret = api_secret;
    }

    async getBalances() {
        let res = await (await fetch('https://api.cryptovoucher.io/merchant/partner', {
            method: 'POST', body: JSON.stringify({
                login: this.api_key,
                password: this.api_secret
            })
        }));

        try {
            res = await res.json();
            return res;
        } catch (e) {
            return Promise.reject(res.statusText)
        }
    }

    async createVoucher(currency, amount) {
        amount = parseInt(amount);
        if (!this.validateAmount(currency, amount)) {
            throw new Error("Invalid amount " + amount + " for currency " + currency)
        } else {
            let res = await (await fetch('https://api.cryptovoucher.io/merchant/voucher/partner', {
                method: 'POST', body: JSON.stringify({
                    login: this.api_key,
                    password: this.api_secret,
                    amount,
                    currency
                })
            }));

            switch (res.status) {
                case 200:
                case 201:
                    res = await res.json();
                    return res;
                case 401:
                    return Promise.reject("bad credentials");
                case 403:
                    return Promise.reject("insufficient fund or balance passed currency not exists");
                default:
                    return Promise.reject(" internal server error");
            }
        }
    }

    static validateAmount(currency, amount) {
        switch (currency) {
            case "EUR":
            case "GBP":
            case "USD":
                return amount >= 10 && amount <= 500;
            case "CAD":
                return amount >= 25 && amount <= 500;
            case "PLN":
                return amount >= 50 && amount <= 500;
            default:
                throw new Error("Invalid currency: " + currency)
        }
    }

    static version() {
        console.log(require('./package').version)
    }
}

module.exports = Cryptovouvher;