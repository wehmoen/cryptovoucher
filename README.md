# Cryptovoucher.io 

This is an unofficial API wrapper for cryptovoucher.io.

### Installation

`npm i cryptovoucher --save`

### Usage

```javascript
const Cryptovoucher = require("cryptovoucher");

const cryptovoucher = new Cryptovoucher("API_KEY","API_SECRET")

// Receive fiat balances

cryptovoucher.getBalances().then(balances => {
    console.log(balances);
}).catch(error => {
    console.log(error);
});

// Create a new voucher
// Valid currencies are: EUR, GBP, USD, CAD and PLN
cryptovoucher.createVoucher("currency", amount).then(voucher => {
    console.log(voucher.code)
}).catch(error => {
    console.log(error)
});

// Check whether a voucher is available for a certain currency and amount
// Throws an error if an invalid currency is passed

let available = cryptovoucher.validateAmount("currency", amount);
if (available === true) {
    console.log("Voucher available");
} else {
    console.log("Voucher not available");
}

// Print the current version

cryptovoucher.version();
```